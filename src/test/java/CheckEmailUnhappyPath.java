
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CheckEmailUnhappyPath {

    private Email email;

    @BeforeClass
    public void initData() {
        email = new Email();
    }

    @DataProvider
    public Object[][] EmailUnhappyPathProvider() {
        return new Object[][] {
                {
                        new String[] {
                                "user",
                                "user!epam.com",
                                "user@epam!com",
                                "user@epam",
                                "us@epam.com",
                                "user+@epam.com",
                                "User-@epam.com",
                                "UserUserUserUserUserUserUserUser@epam.com",
                                "user@e.com",
                                "user@epamepam.com",
                                "user@epam-e.com",
                                "user@epam.c",
                                "user@epam.comcom",
                                "user@epam.COM",
                                "User21@Epam.com",
                        }
                }
        };
    }

    @Test(dataProvider = "EmailUnhappyPathProvider")
    public void EmailUnhappyPathTest(String[] Email) {
        for (String temp : Email) {
            boolean valid = email.isEmailCorrect(temp);
            try {  System.out.println("E-mail: " + temp + " -> " + valid);
                Assert.assertFalse(valid);}
            catch (AssertionError e){System.out.println("E-mail: " + temp + " -> " + false);}
        }
    }
}
