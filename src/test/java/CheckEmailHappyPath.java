
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class CheckEmailHappyPath {

  private Email email;

    @BeforeClass
    public void initData() {email = new Email();
    }

    @DataProvider
    public Object[][] EmailHappyPathProvider() {
        return new Object[][] {
                {
                        new String[] {
                                "user@epam.com",
                                "User@epam.com",
                                "User21@epam.com",
                                "User21_@epam.com",
                                "User21_!@epam.com",
                                "User21@EPAM.com",
                                "User21@Epam.com",
                                "User21@21.com",
                                "user@epam.COM",
                   }
                }
        };
    }

    @Test(dataProvider = "EmailHappyPathProvider")
    public void EmailHappyPathTest(String[] Email) {
        for (String temp : Email) {
            boolean valid = email.isEmailCorrect(temp);
            if (valid) {System.out.println("E-mail: " + temp + " -> " + valid);
                Assert.assertTrue(valid);}
            else {System.out.println("E-mail: " + temp + " -> " + valid);}
        }
    }
}
