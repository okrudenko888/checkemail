package seleniumBaseApi;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
import pages.ButtonPage;
import java.util.concurrent.TimeUnit;
import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class ButtonPageTest {
    ButtonPage buttonPage;
    private WebDriver driver;


    @BeforeTest
    public void profileSetUp(){
        chromedriver().setup();
    }

    @BeforeSuite
    public void testsSetUp(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        buttonPage = new ButtonPage(driver);
        driver.get(ButtonPage.FORMY_URL);
    }

    @AfterTest
        public void tearDown(){
        driver.quit();
    }

    @Test(priority = 1)
    public void verifyButtonPrimaryIsClickable() {
        buttonPage.clickBtn(buttonPage.btnPrimary);
    }

    @Test(priority = 2)
    public void verifyButtonSuccessIsClickable() {
        buttonPage.clickBtn(buttonPage.btnSuccess);
    }

    @Test(priority = 3)
    public void verifyButtonInfoIsClickable() {
        buttonPage.clickBtn(buttonPage.btnInfo);
    }

    @Test(priority = 4)
    public void verifyButtonWarningIsClickable() {
        buttonPage.clickBtn(buttonPage.btnWarning);
    }

    @Test(priority = 5)
    public void verifyButtonDangerIsClickable() {
        buttonPage.clickBtn(buttonPage.btnDanger);
    }

    @Test(priority = 6)
    public void verifyButtonLinkIsClickable() {
        buttonPage.clickBtn(buttonPage.btnLink);
    }

    @Test(priority = 7)
    public void verifyButtonLeftIsClickable() {
        buttonPage.clickBtn(buttonPage.btnLeft);
    }

    @Test(priority = 8)
    public void verifyButtonMiddleIsClickable() {
        buttonPage.clickBtn(buttonPage.btnMiddle);
    }

    @Test(priority = 9)
    public void verifyButtonRightIsClickable() {
        buttonPage.clickBtn(buttonPage.btnRight);
    }

    @Test(priority = 10)
    public void verifyButtonValueOneIsClickable() {
        buttonPage.clickBtn(buttonPage.btnValue1);
    }

    @Test(priority = 11)
    public void verifyButtonValueTwoIsClickable() {
        buttonPage.clickBtn(buttonPage.btnValue2);
    }

    @Test(priority = 12)
    public void verifyButtonDropdownIsClickable() {
        buttonPage.clickBtn(buttonPage.btnDropdown);
        buttonPage.clickBtn(buttonPage.dropdownMenuLink1);
        buttonPage.clickBtn(buttonPage.btnDropdown);
        buttonPage.clickBtn(buttonPage.dropdownMenuLink2);
    }
}
