package seleniumBaseApi;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
import java.util.concurrent.TimeUnit;
import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import pages.CheckboxPage;

public class CheckboxPageTest {

    CheckboxPage checkboxPage;
    private WebDriver driver;

    @BeforeTest
    public void profileSetUp(){
        chromedriver().setup();
    }

    @BeforeMethod
    public void testsSetUp(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        checkboxPage = new CheckboxPage(driver);
        driver.get(CheckboxPage.FORMY_URL);
    }


    @AfterTest
    public void tearDown(){
        driver.quit();
    }

    @DataProvider
    public Object[][] CheckboxProviderXpath() {
        return new Object[][] {checkboxPage.getCheckboxListXpath().toArray()};
    }

    @DataProvider
    public Object[][] CheckboxProviderCSS() {
        return new Object[][] {checkboxPage.getCheckboxListCSS().toArray()};
    }
    @Test
    public void checkboxIsClickable() {
        checkboxPage.clickCheckboxXpath();
    }

    @Test(dataProvider = "CheckboxProviderXpath")
    public void verifyByXpathCheckboxIsClickable() {
        checkboxPage.clickCheckboxXpath();

    }

    @Test(dataProvider = "CheckboxProviderCSS")
    public void verifyByCssCheckboxIsClickable() {
        checkboxPage.clickCheckboxCSS();

    }

}
