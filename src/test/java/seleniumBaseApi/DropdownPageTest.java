package seleniumBaseApi;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.DropdownPage;

import java.util.concurrent.TimeUnit;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class DropdownPageTest {

    DropdownPage dropdownPage;
    private WebDriver driver;

    @BeforeTest
    public void profileSetUp(){
        chromedriver().setup();
    }

    @BeforeMethod
    public void testsSetUp(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        dropdownPage = new DropdownPage(driver);
        driver.get(DropdownPage.FORMY_URL);
    }

    @Test
    public void verifyDropdownIsClickable() {
        dropdownPage.clickDropdown(dropdownPage.dropdownXpath);
        dropdownPage.clickDropdown(dropdownPage.dropdownItemXpath);
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
     //   Assert.assertTrue(driver.getCurrentUrl().contains(DropdownPage.XPATH_QUERY));
        driver.navigate().back();
        Assert.assertTrue(driver.getCurrentUrl().contains(DropdownPage.FORMY_URL));

    }

}
