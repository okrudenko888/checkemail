package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ButtonPage {

    public static final String FORMY_URL = "https://formy-project.herokuapp.com/buttons";
    By btn = By.xpath("//*[contains(@class, 'btn-lg')]");
    public By btnPrimary = By.xpath("//button[text()='Primary']");
    public By btnSuccess = By.xpath("//button[text()='Success']");
    public By btnInfo = By.xpath("//button[text()='Info']");
    public By btnWarning = By.xpath("//button[text()='Warning']");
    public By btnDanger = By.xpath("//button[text()='Danger']");
    public By btnLink = By.xpath("//button[text()='Link']");
    public By btnLeft = By.xpath("//button[text()='Left']");
    public By btnMiddle = By.xpath("//button[text()='Middle']");
    public By btnRight = By.xpath("//button[text()='Right']");
    public By btnValue1 = By.xpath("//button[text()='1']");
    public By btnValue2 = By.xpath("//button[text()='2']");
    public By btnDropdown = By.xpath("//button[contains(text(), 'Dropdown')]");
    public By dropdownMenuLink1 = By.xpath("//a[contains(text(), 'Dropdown link 1')]");
    public By dropdownMenuLink2 = By.xpath("//a[contains(text(), 'Dropdown link 2')]");

    WebDriver driver;

    public ButtonPage (WebDriver driver) {
        this.driver=driver;
    }

    public void clickBtn(By by) {
        driver.findElement(by).click();
    }

}
