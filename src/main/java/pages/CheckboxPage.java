package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;

public class CheckboxPage {

    public static final String FORMY_URL = "https://formy-project.herokuapp.com/checkbox";
    public By checkboxXpath = By.xpath("//input[@type='checkbox']");
    public By checkboxCSS = By.cssSelector("input[type='checkbox']");

    WebDriver driver;

    public CheckboxPage (WebDriver driver) {
        this.driver=driver;
    }

    List<WebElement> checkboxListXpath = driver.findElements(checkboxXpath);
    List<WebElement> checkboxListCSS = driver.findElements(checkboxCSS);

    public List<WebElement> getCheckboxListXpath(){
        return checkboxListXpath;
    }
    public void clickCheckboxXpath() {
        for(WebElement e : checkboxListXpath) {
            driver.findElement((By) e).click();
        }
    }

    public List<WebElement> getCheckboxListCSS(){
        return checkboxListCSS;
    }
    public void clickCheckboxCSS() {
        for(WebElement e : checkboxListCSS) {
            driver.findElement((By) e).click();
        }
    }

}
