package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class DropdownPage {

    public static final String FORMY_URL = "https://formy-project.herokuapp.com/dropdown";
    public By dropdownXpath = By.xpath("//button[contains(@class, 'dropdown-toggle')]");
    public By dropdownItemXpath = By.xpath("//div[@class='dropdown-menu show']//a[text()='Checkbox']");
    public static final String XPATH_QUERY = "сheckbox";

    WebDriver driver;
    public DropdownPage (WebDriver driver) {
        this.driver=driver;
    }

    public void clickDropdown(By by) {
        driver.findElement(by).click();
    }


}
